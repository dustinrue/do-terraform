terraform {
	backend "http" {
		address = "https://gitlab.com/api/v4/projects/39299669/terraform/state/do-software"
		lock_address = "https://gitlab.com/api/v4/projects/39299669/terraform/state/do-software/lock"
		unlock_address = "https://gitlab.com/api/v4/projects/39299669/terraform/state/do-software/lock"
		lock_method = "POST"
		unlock_method = "DELETE"
		retry_wait_min = "5"
	}
}

provider "helm" {
  kubernetes {
    config_path = var.kubeconfig
  }
}

provider "kubernetes" {
	config_path = var.kubeconfig
}
