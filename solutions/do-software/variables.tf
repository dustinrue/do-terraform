
variable "kubeconfig" {
	default = "~/.kube/config"

	validation {
		condition = fileexists(var.kubeconfig)
		error_message = "Looks like you need to save your kubeconfig file yet! Try doctl kubernetes cluster kubeconfig show kubernetes1 > ~/.kube/digitalocean or getting it from your Digital Ocean dashboard."
	}
}

variable "service-options" {
	type = any
	description = "Configure what services are enabled"
	default = {
		ingress-nginx = {
		  enabled = true
			namespace = "kube-system"
			values = {
				controller = {
					config = {
    				proxy-buffer-size = "16k"
					}
					ingressClassResource = {
						default = true
					}
				}
			}
		}

		metrics-server = {
			enabled = true
			values = {}
		}

		cert-manager = {
			enabled = true
			values = {
				installCRDs = true
			}
		}
	}
}
