module "kubernetes-services" {
	source = "../../modules/cluster-services"
	service-options = var.service-options
}
