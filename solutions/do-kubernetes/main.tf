module "network" {
	source = "../../modules/digitalocean/networking"
	network_name = "vpc1"
	network_region = "nyc1"
	network_cidr = "10.11.0.0/16"
}

module "kubernetes" {
	source = "../../modules/digitalocean/kubernetes"
	network_id = module.network.network_id
	node_count = "1"
}
