variable "values" {
	type = map
	default = {}
}

variable "namespace" {
	type = string
	default = "default"
}
