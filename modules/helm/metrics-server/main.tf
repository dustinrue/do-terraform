resource "helm_release" "metrics-server" {
  name       = "metrics-server"
  repository = "https://kubernetes-sigs.github.io/metrics-server/"
  chart      = "metrics-server"
  version    = "3.8.3"
  namespace  = "default"

  values = [
    templatefile("${path.module}/templates/metrics-server.tftpl", {
    })
  ]
}
