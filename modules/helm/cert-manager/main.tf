resource "kubernetes_namespace" "cert-manager" {
  metadata {
    name = "cert-manager"
    labels = {
      name = "cert-manager"
    }
  }
}

resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.11.0"
  namespace  = "cert-manager"

  values = [
    templatefile("${path.module}/templates/cert-manager.tftpl", {
    })
  ]

  depends_on = [
    kubernetes_namespace.cert-manager
  ]
}
