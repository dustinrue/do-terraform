resource "digitalocean_kubernetes_cluster" "kubernetes" {
  name   = var.cluster_name
  region = var.network_region
  version = var.cluster_version
	vpc_uuid = var.network_id

  node_pool {
    name       = var.nodepool_name
    size       = var.node_size
    node_count = var.node_count
  }
}
