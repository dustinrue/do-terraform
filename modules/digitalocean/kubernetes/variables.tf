variable "network_id" {
	default = ""
	description = "Network ID of the VPC to place the cluster into"
}

variable "network_region" {
	default = "nyc1"
	description = "Region to put the Kubernetes cluster into"
}

variable "cluster_name" {
	default = "kubernetes1"
	description = "Name for the Kubernetes cluster"
}

variable "cluster_version" {
	# Grab the latest version slug from `doctl kubernetes options versions`
	default = "1.24.4-do.0"
}

variable "node_size" {
	default = "s-2vcpu-2gb-intel"
}

variable "node_count" {
	default = "1"
}

variable "nodepool_name" {
	default = "nodepool1"
}
