output "network_id" {
	value = resource.digitalocean_vpc.vpc.id
}

output "network_urn" {
	value = resource.digitalocean_vpc.vpc.urn
}
