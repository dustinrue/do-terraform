resource "digitalocean_vpc" "vpc" {
  name     = var.network_name
  region   = var.network_region
  ip_range = var.network_cidr
}
