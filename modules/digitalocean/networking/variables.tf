variable "network_name" {
	default = "vpc1"
}

variable "network_region" {
	default = "nyc1"
}

variable "network_cidr" {
	default = "10.10.10.0/24"
}
