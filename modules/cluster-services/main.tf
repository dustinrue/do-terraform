module "ingress-nginx" {
  count  = var.service-options.ingress-nginx.enabled ? 1 : 0
  source = "https://gitlab.com/dustinrue/do-terraform/-/archive/main/do-terraform-main.zip//do-terraform-main/modules/helm/ingress-nginx"
  namespace = var.service-options.ingress-nginx.namespace
  values = var.service-options.ingress-nginx.values
}

module "cert-manager" {
  count  = var.service-options.cert-manager.enabled ? 1 : 0
  source = "https://gitlab.com/dustinrue/do-terraform/-/archive/main/do-terraform-main.zip//do-terraform-main/modules/helm/cert-manager"
  values = var.service-options.cert-manager.values
}

module "metrics-server" {
  count  = var.service-options.metrics-server.enabled ? 1 : 0
  source = "https://gitlab.com/dustinrue/do-terraform/-/archive/main/do-terraform-main.zip//do-terraform-main/modules/helm/metrics-server"
}
