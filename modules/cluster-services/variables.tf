variable "service-options" {
	type = any # avoid issues with terraform falsely accusing you of not sending in a map
	description = "Configure what services are enabled"
	default = {
		ingress-nginx = {
		  enabled = true
			namespace = "kube-system"
			values = {}
	        }
		metrics-server = {
			enabled = true
			values = {}
		}
		cert-manager = {
			enabled = true
			values = {}
		}
	}
}
