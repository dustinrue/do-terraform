# do-terraform

This repository offers a sort of advanced method of doing terraform that has been simplified to remove a lot of variable settings so that how to use modules is maybe more clear. The intention is to show the way modules can be used and chained together to create a full solution. It also, as a by product of being hosted on gitlab.com, shows how to store state information in gitlab itself. 

## How to use

To use this, you need to have:

* gitlab.com account
* digitalocean account. You can use my referral code if you don't have an account - [https://m.do.co/c/5016d3cc9b25](https://m.do.co/c/5016d3cc9b25)
* doctl (brew install doctl)
* terraform 1.2.9+
* At least some experience using terraform and command line

Start by creating or logging into your gitlab.com account. Create an API token at [https://gitlab.com/-/profile/personal_access_tokens](https://gitlab.com/-/profile/personal_access_tokens). Scope the token to "API". Copy the token out and add it as an exported variable in your .bashrc (or .zshrc, whatever shell you use). In Bash this would look like this:

```
export TF_HTTP_USERNAME=dustinrue
export TF_HTTP_PASSWORD=<redacted>
```

Next, create a digital ocean token at [https://cloud.digitalocean.com/account/api/tokens](https://cloud.digitalocean.com/account/api/tokens) with write access. Save this as a variable as well:

```
export DIGITALOCEAN_TOKEN=<redacted>
```

Source your profile file or close and reopen you terminal session.

Fork this repository to your gitlab.com instance if you haven't already. **Update the provider.tf file in each solutions directory to point at your account**. Replace the number in each variable with the project ID for your forked copy. Your changes might look like:

```
terraform {
	backend "http" {
		address = "https://gitlab.com/api/v4/projects/123456/terraform/state/do-kubernetes"
		lock_address = "https://gitlab.com/api/v4/projects/123456/terraform/state/do-kubernetes/lock"
		unlock_address = "https://gitlab.com/api/v4/projects/123456/terraform/state/do-kubernetes/lock"
		lock_method = "POST"
		unlock_method = "DELETE"
		retry_wait_min = "5"
	}
}
```

Repeat this for each providers.tf file you find.

### Creating Base Infrastructure

Enter the do-kubernetes directory and issue `terraform init` followed by `terraform apply`. When prompted, enter yes and press enter to create the base infrastructure.

When it is complete get your Kubernetes config using doctl. If you haven't used it before you will need to run `doctl auth login` and paste in the token you generated before. To save your kube config issue `doctl kubernetes cluster kubeconfig show kubernetes1 > ~/.kube/digitalocean`.

Out of the box, you will get a non HA Kubernetes cluster using `s-2vcpu-2gb-intel` sized machine. This is just enough to install a few base services and you will likely want to use a different size of instance or multiple of them for hosting actual software.

### Installing Base Services

This repository separates the installation of Kubernetes software from the base infrastructure. This is to avoid some odd race condition type issues that arise when you combine Kubernetes infrastructure and software.

To apply the base services enter the do-software directory and issue `terraform init` followed by `terraform apply`. Like before enter yes and press enter to complete the base software installation. 

## Destroying

Enter the do-kubernetes directory and issue `terraform destroy`. This will remove all resources. You may need to manually delete the Load Balancer created by ingress-nginx.
